package main

import (
	"fmt"
	"log"
	"strings"
)

var (
	cSysFSPrefix = "/sys/class/power_supply"
)

// ChargeControl controls the charging
type ChargeControl struct {
	BatteryLocation string
	ChargerLocation string

	ControlType uint16
	ControlFile string
}

func NewChargeControl(charger, battery string) ChargeControl {
	return ChargeControl{
		BatteryLocation: battery,
		ChargerLocation: charger,
		ControlType:     1,
		ControlFile:     "constant_charge_current",
	}
}

func (cc ChargeControl) GetPowerSupplyState() bool {
	filename := fmt.Sprintf("%s/%s/%s", cSysFSPrefix, cc.ChargerLocation, "online")
	b, err := ioReadBool(filename)
	if err != nil {
		log.Fatalln(err.Error())
	}
	return b
}

func (cc ChargeControl) GetMaxChargeCurrent() int64 {
	filename := fmt.Sprintf("%s/%s/%s", cSysFSPrefix, cc.ChargerLocation, cc.ControlFile)
	i, err := ioReadInt(filename, 10)
	if err != nil {
		log.Fatalln(err.Error())
	}
	return i
}

func (cc ChargeControl) GetChargingState() bool {
	maxCurrent := cc.GetMaxChargeCurrent()
	if maxCurrent == 0 {
		return false
	}
	currentSupply := cc.GetPowerSupplyState()
	if !currentSupply {
		return false
	}
	if cc.IsFull() {
		return false
	}
	return true
}

func (cc ChargeControl) GetBatteryState() int64 {
	filename := fmt.Sprintf("%s/%s/%s", cSysFSPrefix, cc.BatteryLocation, "capacity")
	i, err := ioReadInt(filename, 10)
	if err != nil {
		log.Fatalln(err.Error())
	}
	return i
}

func (cc ChargeControl) SetMacChargeCurrent(newMax int64) {
	filename := fmt.Sprintf("%s/%s/%s", cSysFSPrefix, cc.ChargerLocation, cc.ControlFile)
	err := ioWriteInt(filename, newMax, 10)
	if err != nil {
		log.Fatalln(err.Error())
	}
}

func (cc ChargeControl) IsFull() bool {
	filename := fmt.Sprintf("%s/%s/%s", cSysFSPrefix, cc.ChargerLocation, "status")
	s, err := ioReadString(filename)
	if err != nil {
		log.Fatalln(err.Error())
	}
	s = strings.Trim(s, " \n\r\t")
	if s == "Full" {
		return true
	}
	return false
}
