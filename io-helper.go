package main

import (
	"os"
	"strconv"
	"strings"
)

type boolFormat uint8

const (
	boolInt boolFormat = iota
	boolString
	boolStringSingle
)

func ioReadString(path string) (string, error) {
	data, err := os.ReadFile(path)
	if err != nil {
		return "", err
	}

	return string(data), nil
}

func ioReadBool(path string) (bool, error) {
	data, err := ioReadString(path)
	data = strings.Trim(data, " \n\r\t")
	if err != nil {
		return false, err
	}

	b, err := strconv.ParseBool(data)
	if err != nil {
		return false, err
	}

	return b, nil
}

func ioReadInt(path string, base int) (int64, error) {
	data, err := ioReadString(path)
	data = strings.Trim(data, " \n\r\t")
	if err != nil {
		return 0, err
	}

	i, err := strconv.ParseInt(string(data), base, 64)
	if err != nil {
		return 0, err
	}

	return i, nil
}

func ioReadFloat(path string) (float64, error) {
	data, err := ioReadString(path)
	data = strings.Trim(data, " \n\r\t")
	if err != nil {
		return 0, err
	}

	f, err := strconv.ParseFloat(data, 64)
	if err != nil {
		return 0, err
	}

	return f, nil
}

func ioWriteString(path, data string) error {
	err := os.WriteFile(path, []byte(data), 0664)
	return err
}

func ioWriteBool(path string, data bool, format boolFormat) error {
	d := ""
	switch format {
	case boolInt:
		if data {
			d = "1"
		} else {
			d = "0"
		}

	case boolString:
		if data {
			d = "true"
		} else {
			d = "false"
		}

	case boolStringSingle:
		if data {
			d = "t"
		} else {
			d = "f"
		}
	}

	return ioWriteString(path, d)
}

func ioWriteInt(path string, data int64, base int) error {
	return ioWriteString(path, strconv.FormatInt(data, base))
}
