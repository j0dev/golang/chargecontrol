package main

import "log"

func ControlLogic(config Config, controller ChargeControl) {
	// get current state
	battery := controller.GetBatteryState()
	maxCurrent := controller.GetMaxChargeCurrent()
	charging := controller.GetChargingState()
	power := controller.GetPowerSupplyState()
	chargingCurrent := config.GetSelectedMaxCurrent()

	// Control logic
	if !power {
		if maxCurrent > 0 {
			// disable charging when disconnected
			log.Printf("Disabling charing\n")
			controller.SetMacChargeCurrent(0)
			return
		}
	} else if !charging {
		if maxCurrent > 0 {
			// disable charging when full
			log.Printf("Disabling charing\n")
			controller.SetMacChargeCurrent(0)
			return

		} else if battery <= config.StartPercent {
			// enable charging if below threshold
			log.Printf("Enabling charging with max of %dmA\n", config.GetSelectedMaxCurrent()/1000)
			controller.SetMacChargeCurrent(chargingCurrent)
			return
		}

	} else if battery >= config.MaxCharge {
		// disable charging if above set maximum charge
		log.Printf("Disabling charing\n")
		controller.SetMacChargeCurrent(0)
		return
	}
	log.Printf("No changes needed\n")
}
