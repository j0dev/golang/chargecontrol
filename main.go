package main

import (
	"fmt"
	"log"
	"os"
)

func main() {
	config, err := LoadConfig(DefaultConfigFileLocation)
	if err != nil {
		if _, ok := err.(*os.PathError); ok {
			log.Println("No configuration file found, writing default configuration.")
			WriteDefaultConfig(DefaultConfigFileLocation)
			log.Printf("Default configuration written to %s,\nPlease configure your settings!", defaultConfigFileContents)
			os.Exit(0)
		}
		log.Fatal(err.Error())
	}

	controller := NewChargeControl(config.Charger, config.Battery)

	PrintState(config, controller)
	ControlLogic(config, controller)
}

func PrintState(config Config, controller ChargeControl) {
	// get current state
	battery := controller.GetBatteryState()
	maxCurrent := controller.GetMaxChargeCurrent()
	charging := controller.GetChargingState()
	power := controller.GetPowerSupplyState()

	stateString := ""
	if charging {
		if config.MaxCharge < 100 {
			stateString = fmt.Sprintf("charging at %dmA to %d%%", maxCurrent/1000, config.MaxCharge)
		} else {
			stateString = fmt.Sprintf("charging at %dmA", maxCurrent/1000)
		}
	} else {
		stateString = "discharging"
	}

	chargeString := ""
	if power {
		chargeString = "connected"
	} else {
		chargeString = "disconnected"
	}

	log.Printf("Battery state: %d%% %s\n", battery, stateString)
	log.Printf("Charger state: %s\n", chargeString)
}
