package main

import (
	_ "embed"
	"log"
	"os"

	"gopkg.in/yaml.v2"
)

//go:embed chargeControl.yaml
var defaultConfigFileContents []byte
var (
	DefaultConfigFileLocation string = "/etc/chargeControl.yaml"
)

// Config holds the configuration structure
type Config struct {
	Charger string `yaml:"charger"`
	Battery string `yaml:"battery"`

	ChargingProfile  string           `yaml:"charging-profile"`
	ChargingProfiles map[string]int64 `yaml:"charging-profiles"`

	StartPercent int64 `yaml:"start-percent"`
	StopPercent  int64 `yaml:"stop-percent"`
	MaxCharge    int64 `yaml:"max-charge"`
}

func LoadConfig(configFile string) (c Config, err error) {
	data, err := os.ReadFile(configFile)
	if err != nil {
		return
	}

	err = yaml.Unmarshal(data, &c)
	if err != nil {
		log.Fatal(err.Error())
	}

	return c, nil
}

func WriteDefaultConfig(configFile string) {
	file, err := os.OpenFile(configFile, os.O_WRONLY|os.O_CREATE, 0664)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	file.Write(defaultConfigFileContents)
}

func (c Config) GetSelectedMaxCurrent() int64 {
	current, ok := c.ChargingProfiles[c.ChargingProfile]

	if !ok {
		current = 1472000
	}

	return current
}
