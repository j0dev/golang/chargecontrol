Changelog
======

## 1.1.0
Release date: 2024-03-22 13:30

Changes:
- always disable charging when unplugged
- reworked control logic
- made system more inline in controlling via charger connect events


## 1.0.1
Release date: 2024-03-16 23:00

Changes:
- Fixed charging state when full and still connected


## 1.0.0
Release data: 2024-03-16 22:30

First release
