ChargeControl
======

## Description
This tool will turn on and off the charging of the battery by setting a maximum charing current on the charger trough SysFS.

The tool is meant to be run on a timer and charger change event.
It can be configured with battery percent to start, stop after disconnect and maximum charge.


## Configuration
At first run, the tool will create a configuration file in [/etc/chargeControl.yaml](chargeControl.yaml)  
This file is commented and explained inline with comments.


## Installation
1. Download / compile the binary and place it in a directory to execute it from
2. Ensure the ownership of the binary is set to root  
    `chown root <binary_path>`
3. set `setuid` bit on the binary to allow anyone to update the charging controller  
    `chown u+s <binary_path>`
4. Run the binary to generate the config file
5. Change group ownership of the config file to allow users to change the configuration
    `chown root:<user> /etc/chargeControl.yaml`
6. Install timers for automatic updating of the charger controller



## Planned features
- [x] Update maximum current to charge with
- [x] Configurable charger and battery path (support for other devices)
- [ ] Different control modes (e.g. with boolean charging control file)

- [x] Enable when battery is below a set point
- [x] Disable when battery is above a set point and disconnected
- [x] Disable when battery is above a set point

- [x] Predefined current profiles and profile to use option

- [ ] commands for one-time control
    - [ ] force enable / disable charging
    - [ ] override max current profile to use

- [ ] daemon mode?
- [ ] timer examples

- [ ] packaging for distro's

- [ ] user interface
- [ ] phosh quicksetting